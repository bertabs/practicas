#creamos la clase cohe que tendra los atributos de nuestro coche 
class Coche: 
    #establecemos el metodo init que es el constructor de coche
    def __init__(self,color, marca, modelo, matricula, velocidad):
        #definimos los atributos del coche
        self.color = color
        self.marca = marca
        self.modelo = modelo
        self.matricula = matricula
        self.velocidad = velocidad
    #creamos el metodo acelerar
    def acelerar(self):
        #al usar esta funcion a la velocidad se le sumara 1
        self.velocidad = self.velocidad + 1
        return self
    #creamos el metodo acelerar
    def frenar(self):
        #al usar esta funcion a la velocidad se le restara 1
        self.velocidad = self.velocidad - 1
        return self
    
#definimos dos objetos, coche1 y coche2, cada uno con sus aributos
coche1 = Coche("negro", "renault", "captur", "8646JUG", 100)
coche2 = Coche("negro", "audi", "Q5", "7483KGY", 102)
print(coche1.color, coche1.marca, coche1.modelo, coche1.matricula, coche1.velocidad)