#importamos la libreria unittest para hacer los test unitarios
import unittest
#importamos la clase a la que queremos pasar las pruebas
from practica_1 import Coche

#creamos la clase que va a pasar los test 
class TestCoche(unittest.TestCase):
    #metodo que testea el metodo acelerar
    def test_acelerar(self):
        #creamos el objeto "cochecito" para comparar en los test
        cochecito = Coche("negro", "renault", "captur", "8646JUG", 100)
        #creamos una variable que sera nuestro objeto con el metodo que queremos testear 
        a = cochecito.acelerar()
        #si la velocidad de a != 101, el test lanzara un error 
        #debido a que este esta utilizando el metodo assertEqual de unittest
        self.assertEqual(a.velocidad, 101)

    #metodo que testea el metodo frenar
    def test_frenar(self):
        #creamos el objeto "cochaco" para comparar en los test
        cochaco = Coche("negro", "audi", "Q5", "7483KGY", 102)
        #creamos una variable que sera nuestro objeto con el metodo que queremos testear 
        f = cochaco.frenar()
        #si la velocidad de a != 101, el test lanzara un error 
        #debido a que este esta utilizando el metodo assertEqual de unittest
        self.assertEqual(f.velocidad, 101)

if __name__ == '__main__':
    unittest.main()
