# creamos la clase persona 
class Persona:
    # es el constructor de la clase, suele inicializar los atrubutos del objeto
    def __init__(self, n, a, nac, DNI):
        self._nombre = n
        self._apellidos = a
        self._nacimiento = nac
        self._DNI = DNI

    # Modifica el valor del atributo _nombre
    def setNom(self, n):
        self._nombre = n

    # Devuelve el valor del atributo _nombre
    def getNom(self):
        return self._nombre
    
    def setAp(self, a):
        self._apellidos = a

    def getAp(self):
        return self._apellidos
    
    def setNac(self, nac):
        self._nacimiento = nac

    def getNac(self):
        return self._nacimiento
    
    def setDNI(self, DNI):
        self._DNI = DNI

    def getDNI(self):
        return self._DNI
    
    #metodo que imprime la informacion de la "persona"
    def __str__(self):
        return f"Nombre: {self._nombre}, Apellidos: {self._apellidos}, Fecha de nacimiento: {self._nacimiento}, DNI: {self._DNI}"
#creamos la clase paciente que es una clase hija de Persona
class Paciente(Persona):
    #los mismos atributos que persona pero añadiendo hc
    def __init__(self, n, a, nac, DNI, hc):
        #con el super, inicializamos directamente los atributos que estaban en persona
        super().__init__(n, a, nac, DNI)
        self._historial_clinico = hc

    def ver_historial_clinico(self):
        #nos muetra el historial clinico de la persona
        return f"El historial clínico de {super().getNom()} es: {self._historial_clinico}"
    
#creamos la clase medico que es una clase hija de persona
class Medico(Persona):
    def __init__(self, n, a, nac, DNI, e, ci):
        super().__init__(n, a, nac, DNI)
        self._especialidad = e
        self._citas = ci
    
    def consultar_citas(self):
        #nos muestra las citas que tiene que atender el medico
        return f"Las citas de {super().getNom()} son: {self._citas}"
    
    
    