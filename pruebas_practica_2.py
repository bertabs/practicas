#importamos la libreria unittest para hacer los test unitarios
import unittest
#importamos la clase a la que queremos pasar las pruebas
from practica_2 import Persona, Paciente, Medico

#creamos la clase que va a pasar los test 
class Test_persona(unittest.TestCase):
    #test __str__
    def test_str(self):
        #creamos un objeto de la clase que vamos a usar para este test
        persona1 = Persona("Adrián", "Prieto", "27/nov/2005", "5372002J")
        #usamos el metodo assertEqual para comprobar que esta clase hace lo que queremos
        self.assertEqual(str(persona1), "Nombre: Adrián, Apellidos: Prieto, Fecha de nacimiento: 27/nov/2005, DNI: 5372002J")
class Test_paciente(unittest.TestCase):
    def test_ver_historial_clinico(self):
        #creamos un objeto de la clase que vamos a usar para este test
        paciente2 = Paciente("Adrián", "Prieto", "27/nov/2005", "5372002J", "cancer")
        #usamos el metodo assertEqual para comprobar que el atributo de esta clase hace lo que queremos
        self.assertEqual(str(paciente2.ver_historial_clinico()), "El historial clínico de Adrián es: cancer")
class Test_medico(unittest.TestCase):
    def test_consultar_citas(self):
        #creamos un objeto de la clase que vamos a usar para este test
        medico1 = Medico("Adriano Pablo", "González", "23/nov/2005", "345678945L", "neurocirujano", "ver a Adrian, ver a Berta")
        #usamos el metodo assertEqual para comprobar que el atributo de esta clase hace lo que queremos
        self.assertEqual(str(medico1.consultar_citas()), "Las citas de Adriano Pablo son: ver a Adrian, ver a Berta")
if __name__ == '__main__':
    unittest.main()
